let elements;
let img;
let slider;
let button;
let parrafo;

let d;
let sentido; 
let valor;
let r;
let valorInverso;
let canvas;


function setup() {
    canvas = createCanvas(500, 500);
    img = createImg("https://i.pinimg.com/originals/ba/61/d7/ba61d7f320605f7501234cae98e5d5e1.png");
    slider = createSlider(0, 255, 100);
    button = createImg('images/botonClick.png');
    parrafo = createP('Batman...');

    elements = [canvas.parent('myContainer'), 
              img.parent('myContainer'),
              slider.parent('myContainer'), 
              button.parent('myContainer'), 
              parrafo.parent('myContainer')];

    elements[0].position(340, 70);
    elements[1].position(10, 60);
    elements[1].size(375, 621);
    elements[2].position(870, 100);
    elements[2].style('width', '200px');
    elements[3].position(870, 200);
    elements[3].size(100, 100);
    elements[3].mousePressed(changeFondo);
    elements[4].position(300, 850);
    elements[4].style('font-size', r*0.7 +'px');
    elements[4].style('margin-top', -r*0.7 +'px');


    d = 20.0; 
    sentido = 1;
    valor = color('#8408ff');
    valorInverso = color(255 - red(valor), 255 - green(valor), 255 - blue(valor));
}


function draw() {
  background(valor);
  noStroke();
  let val = slider.value();
  fill(255, val);
  ellipse(0, 250, d, d); 
  ellipse(500, 250, d, d);
  ellipse(250, 0, d, d);  
  ellipse(250, 500, d, d); 
  
  d += 8 * sentido;
  
  if (d > 500 || d < 20) { 
    sentido *= -1; 
  }
  
  fill(valorInverso);
  ellipse(width/2, height/2, 12, 12);
  
  if (mouseIsPressed && mouseX > 0 && mouseX < canvas.width && mouseY > 0 && mouseY < canvas.height) {
    r = dist(width/2, height/2, mouseX, mouseY);
    fill(valorInverso);
    ellipse(mouseX, mouseY, 12, 12);
    stroke(valorInverso);
    strokeWeight(3);
    line(mouseX, mouseY, width/2, height/2);
  }

  elements[4].style('font-size', r*0.7 +'px');
  elements[4].style('margin-top', -r*0.7 +'px');
}

function changeFondo() {
  let listaColores = [
      color('rgb(235, 50, 123)'), 
      color('rgb(16, 73, 158)'), 
      color('rgb(0, 99, 21)'), 
      color('rgb(117, 68, 68)'), 
      color('rgb(255, 179, 65)'),
      color('rgb(61, 61, 61)')
    ];
    
  valor = listaColores[int(random(listaColores.length))];
  valorInverso = color(255 - red(valor), 255 - green(valor), 255 - blue(valor));
}